import os


ONEMB = 1024*1024

def test_fpath(abs_fpath):
    exts = ['.heic', '.jpg', '.jpeg', '.gif', '.img', '.tif', '.tiff', '.psd', '.pdf', '.png']

    if not os.path.isfile(abs_fpath):
        return False
    if not os.access(abs_fpath, os.R_OK):
        return False
    if os.path.splitext(abs_fpath)[1].lower() not in exts:
        return False

    return True




def walk_path(start_path):
    for ppath, dnames, fnames in os.walk(start_path):
        image_counter = 0
        abs_ppath = os.path.abspath(ppath)
        for fname in fnames:
            abs_fpath = os.path.abspath(os.path.join(ppath, fname))
            if not test_fpath(abs_fpath):
                continue

            image_counter += 1

        yield (abs_ppath, image_counter)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('start_path', help='Start path')
    args = parser.parse_args()

    if not os.path.isdir(args.start_path):
        print('Start path not a directory, exiting...')
        sys.exit(1)

    for path_details in walk_path(args.start_path):
        abs_ppath, image_counter = path_details
        if image_counter >= 4:
            print('{}: {}'.format(image_counter, abs_ppath))

