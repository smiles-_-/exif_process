import datetime
import exif
import hashlib
import os
import shutil


ONEMB = 1024*1024

def get_md5(fpath, bsize=ONEMB):
    md5 = hashlib.md5()
    with open(fpath, 'rb') as f:
        while True:
            data = f.read(bsize)
            if not data:
                break
            md5.update(data)
    return md5.hexdigest()

def get_exif_data(exif_fname):
    year = None
    month = None

    with open(exif_fname, 'rb') as f:
        try:
            image_file = exif.Image(f)
        except:
            print('File Exceptioned: {}'.format(exif_fname))
            return year, month
    #if not image_file.has_exif:
        #print('No EXIF Data: {}'.format(exif_fname))

    if image_file.has_exif:
        if hasattr(image_file, 'datetime'):
            timestamp = image_file.datetime.split(':', 2)
            if len(timestamp) > 2:
                year = timestamp[0]
                month = timestamp[1]
            else:
                year = timestamp[0]
    
            return year, month
    
    timestamp = datetime.datetime.fromtimestamp(os.stat(exif_fname).st_mtime)
    year = str(timestamp.year)
    month = str(timestamp.month).zfill(2)
    
    return year, month


def test_fpath(abs_fpath):
    exts = ['.heic', '.jpg', '.jpeg', '.gif', '.img', '.tif', '.tiff', '.psd', '.pdf', '.png']

    if not os.path.isfile(abs_fpath):
        return False
    if not os.access(abs_fpath, os.R_OK):
        print('Failed Access Test: {}'.format(abs_fpath))
        return False
    if not os.path.getsize(abs_fpath) < 100*ONEMB:
        return False
    if os.path.splitext(abs_fpath)[1].lower() not in exts:
        return False

    return True


def make_dirs(dst_abspath, year, month):
    year_path = os.path.join(dst_abspath, year)
    if not os.path.isdir(year_path):
        os.mkdir(year_path)

    month_path = os.path.join(year_path, month)
    if not os.path.isdir(month_path):
        os.mkdir(month_path)

    move_path = month_path

    return move_path


def walk_path(start_path):
    for ppath, dnames, fnames in os.walk(start_path):
        for fname in fnames:
            abs_fpath = os.path.abspath(os.path.join(ppath, fname))
            if not test_fpath(abs_fpath):
                continue

            yield abs_fpath


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('start_path', help='Start path')
    parser.add_argument('--dst', help='Destination path')
    args = parser.parse_args()

    dst_abspath = os.path.abspath(args.dst)

    if os.path.isdir(args.start_path):
        for exif_fname in walk_path(args.start_path):
            year, month = get_exif_data(exif_fname)
            if not year or not month:
                continue
            move_path = make_dirs(dst_abspath, year, month)
            md5 = get_md5(exif_fname)
            new_fname = '{}_{}'.format(md5, os.path.basename(exif_fname))
            dest = os.path.join(move_path, new_fname)
            #shutil.move(exif_fname, dest)
            shutil.copyfile(exif_fname, dest)
            print('{} -> {}'.format(exif_fname, dest))
    elif os.path.isfile(args.start_path):
        if test_fpath(args.start_path):
            year, month = get_exif_data(args.start_path)
            move_path = make_dirs(dst_abspath, year, month)
            md5 = get_md5(args.start_path)
            new_fname = '{}_{}'.format(md5, os.path.basename(args.start_path))
            dest = os.path.join(move_path, new_fname)
            #shutil.move(args.start_path, dest)
            shutil.copyfile(args.start_path, dest)
            print('{} -> {}'.format(args.start_path, dest))
            #print(args.start_path)
